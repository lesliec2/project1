/**
 * @file main.cpp
 * @author John Schafer
 */


#include "pch.h"
#include <iostream>

// Main library header file
#include "GameApp.h"

wxIMPLEMENT_APP(GameApp);
