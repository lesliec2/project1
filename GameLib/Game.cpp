/**
 * @file Game.cpp
 * @author Colton Leslie
 * @author Connor Chapman
 * @author Arul Srivastava
 */

#include "pch.h"
#include "Game.h"

#include "NullBug.h"
#include "RedundancyBug.h"
#include "GarbageBug.h"
#include "Scoreboard.h"
#include "FeatureBug.h"
#include "Program.h"
#include "IsItemVisitor.h"
//#include "PowerUpVisitor.h"
#include "ProgramAssociationVisitor.h"
#include "vector"

// Test

using namespace std;

/// Game area in virtual pixels
const static int Width = 1250;

/// Game area height in virtual pixels
const static int Height = 1000;
/// power up time
const int PowerUpTime = 80;

/// Scale to shrink to when in shrink mode
const double ShrinkScale = 0.75;

/// The range from the center of a Program that is considered to be clicking
/// on the Program
const double ProgramHitRange = 5.0;

/// Vertical and Horizontal Clipping for Bugs outside of area
const int ClipDistance = 1000;

/// The first font color
const wxColour FontColor = wxColour(225, 225, 225);

/// The second font color
const wxColour FontColor2 = wxColour(200, 0, 0);

/// Program name font size
const int ProgramFontSize = 22;

/// Area if clicked will activate the power up.
const double StartPowerUpWidth = Width-200;

/// Area if clicked will activate the power up.
const double StartPowerUpHeight = 200;

/// Area if clicked will activate the power up.
const double EndPowerUpWidth = 150;

/// Area if clicked will activate the power up.
const double EndPowerUpHeight = 100;

/// The amount of points the power up costs
const int PowerUpCost = 7;

/// Image path wstring for all images
const std::wstring ImagePath = L"images/";

/// The feature sprite image
const std::wstring FeatureBugSpriteImageName = L"feature.png";

/// The feature splat image
const std::wstring FeatureBugSplatImageName = L"feature-splat.png";

/// The garbage bug sprite image
const std::wstring GarbageBugSpriteImageName = L"blue-maize-bug.png";

/// The garbage splat image
const std::wstring GarbageBugSplatImageName = L"blue-maize-splat.png";

/// The null bug sprite image
const std::wstring NullBugSpriteImageName = L"scarlet-gray-bug.png";

/// The null splat image
const std::wstring NullBugSplatImageName = L"scarlet-gray-splat.png";

/// The program sprite image
const std::wstring ProgramSpriteImageName = L"laptop.png";

/// The bug base image
const std::wstring RedundancyFlyBase = L"redundancy-fly-base.png";

/// The bug top image
const std::wstring RedundancyFlyTop = L"redundancy-fly-top.png";

/// The left wing image
const std::wstring RedundancyFlyLeftWing = L"redundancy-fly-lwing.png";

/// The right wing image
const std::wstring RedundancyFlyRightWing = L"redundancy-fly-rwing.png";

/// The splat image
const std::wstring RedundancyFlySplat = L"redundancy-fly-splat.png";

/**
 * Game Constructor.
 */
Game::Game()
{
	mShrinked = false;
    mScoreboard = make_unique<Scoreboard>(this);


	// Feature Bug images
	mImageMap[FeatureBugSpriteImageName] = make_shared<wxImage>(ImagePath + FeatureBugSpriteImageName, wxBITMAP_TYPE_ANY);
	mImageMap[FeatureBugSplatImageName] = make_shared<wxImage>(ImagePath + FeatureBugSplatImageName, wxBITMAP_TYPE_ANY);
	// Garbage Bug images
	mImageMap[GarbageBugSpriteImageName] = make_shared<wxImage>(ImagePath + GarbageBugSpriteImageName, wxBITMAP_TYPE_ANY);
	mImageMap[GarbageBugSplatImageName] = make_shared<wxImage>(ImagePath + GarbageBugSplatImageName, wxBITMAP_TYPE_ANY);
	//Null Bug images
	mImageMap[NullBugSpriteImageName] = make_shared<wxImage>(ImagePath + NullBugSpriteImageName, wxBITMAP_TYPE_ANY);
	mImageMap[NullBugSplatImageName] = make_shared<wxImage>(ImagePath + NullBugSplatImageName, wxBITMAP_TYPE_ANY);
	// Program images
	mImageMap[ProgramSpriteImageName] = make_shared<wxImage>(ImagePath + ProgramSpriteImageName, wxBITMAP_TYPE_ANY);
	// Redundancy Fly images
	mImageMap[RedundancyFlyBase] = make_shared<wxImage>(ImagePath + RedundancyFlyBase, wxBITMAP_TYPE_ANY);
	mImageMap[RedundancyFlyTop] = make_shared<wxImage>(ImagePath + RedundancyFlyTop, wxBITMAP_TYPE_ANY);
	mImageMap[RedundancyFlyLeftWing] = make_shared<wxImage>(ImagePath + RedundancyFlyLeftWing, wxBITMAP_TYPE_ANY);
	mImageMap[RedundancyFlyRightWing] = make_shared<wxImage>(ImagePath + RedundancyFlyRightWing, wxBITMAP_TYPE_ANY);
	mImageMap[RedundancyFlySplat] = make_shared<wxImage>(ImagePath + RedundancyFlySplat, wxBITMAP_TYPE_ANY);

	Load(mLevel);


}


/**
 * Draw the game
 * @param graphics The graphics context to draw to
 * @param width The width of game
 * @param height The height of the game
 */
void Game::OnDraw(const std::shared_ptr<wxGraphicsContext>& graphics, int width, int height)
{
    // Automatic Scaling
    auto scaleX = double(width) / double(Width);
    auto scaleY = double(height) / double(Height);
    mScale = std::min(scaleX, scaleY);

    if(mShrinked)
    {
        mScale *= ShrinkScale;
    }

    mXOffset = (width - Width * mScale) / 2;
    mYOffset = (height - Height * mScale) / 2;

    graphics->PushState();

    graphics->Translate(mXOffset, mYOffset);
    graphics->Scale(mScale, mScale);

    // From here on you are drawing virtual pixels
    graphics->SetPen(*wxBLACK_PEN);
    wxBrush rectBrush(*wxWHITE);
    graphics->SetBrush(rectBrush);
    graphics->SetPen(wxNullGraphicsPen);
    graphics->DrawRectangle(0, 0, Width, Height);

    // Draw the power up
    rectBrush = *wxRED;
    graphics->SetBrush(rectBrush);
    graphics->DrawEllipse(StartPowerUpWidth, StartPowerUpHeight, EndPowerUpWidth, EndPowerUpHeight);
    wxFont labelFont(wxSize(0, ProgramFontSize),
                     wxFONTFAMILY_SWISS,
                     wxFONTSTYLE_NORMAL,
                     wxFONTWEIGHT_BOLD);
	wxFont labelFont2(wxSize(0, 100),
					 wxFONTFAMILY_SWISS,
					 wxFONTSTYLE_NORMAL,
					 wxFONTWEIGHT_BOLD);

    graphics->SetFont(labelFont, FontColor);
    graphics->DrawText(L"POWERUP",Width-180,230);

    if (mScoreboard->GetFixed() >= PowerUpCost)
    {
        graphics->DrawText(L"READY",Width-165,250);
    }


	for (auto item : mItems)
	{
		item->Draw(graphics);
    }

    rectBrush = *wxBLACK;
    if (mShrinked)
    {
        rectBrush = *wxTRANSPARENT_BRUSH;
    }

    graphics->SetPen(*wxBLACK_PEN);
    graphics->SetBrush(rectBrush);
    graphics->SetPen(wxNullGraphicsPen);
    graphics->DrawRectangle(-ClipDistance, -ClipDistance, ClipDistance, Height+(ClipDistance*2));
    graphics->DrawRectangle(Width, -ClipDistance, ClipDistance, Height+ClipDistance*2);
    graphics->DrawRectangle(-ClipDistance, -ClipDistance, Width+ClipDistance*2, ClipDistance);
    graphics->DrawRectangle(-ClipDistance, Height, Width+ClipDistance*2, ClipDistance);


    mScoreboard->Draw(graphics);

	graphics->SetFont(labelFont2, FontColor2);
	if (mDisplayIntro > 0)
	{
		graphics->DrawText(mIntroText, 350, 500);
	}
    graphics->PopState();
}

/**
 * Handle updates for animation
 * @param elapsed The time since the last update
 */
void Game::Update(double elapsed)
{
	if (mPowerUp > 0)
	{
		mPowerUp -= 1;
		return;
	}

	if (mDisplayIntro > 0)
	{
		mDisplayIntro -= 1;
		return;
	}
    if (mItems.size() != 0)
    {
		SetLevelOver();
		if (mOver)
		{
			if (mLevel ==  L"Data/level0.xml")
			{
				mLevel = L"Data/level1.xml";
				const wstring nxt_lvl = L"Data/level1.xml";
				Load( nxt_lvl);
				mOver = false;

			}
			else if (mLevel ==  L"Data/level1.xml")
			{
				mLevel = L"Data/level2.xml";
				const wstring nxt_lvl = L"Data/level2.xml";

				Load( nxt_lvl);
				mOver = false;

			}
			else if (mLevel ==  L"Data/level2.xml")
			{
				mLevel = L"Data/level3.xml";
				const wstring nxt_lvl = L"Data/level3.xml";
				Load( nxt_lvl);
				mOver = false;

			}
			else if (mLevel ==  L"Data/level3.xml")
			{
				mLevel = L"Data/level0.xml";
				const wstring nxt_lvl = L"Data/level0.xml";
				Load( nxt_lvl);
				mOver = false;

			}

		}
        ProgramAssociationVisitor visitorProgram;
        IsItemVisitor visitorItem;
        for (auto item : mItems)
        {
            item->Update(elapsed);
            item->Accept(&visitorItem);
            if (!visitorItem.GetIsProgram())
            {
                item->Accept(&visitorProgram);
                if ((abs(item->GetX()-visitorProgram.GetProgram()->GetX())) < ProgramHitRange
                            && abs((item->GetY()-visitorProgram.GetProgram()->GetY())) < ProgramHitRange
                            && !visitorItem.GetBug()->GetMissed())
                {
                    visitorItem.GetBug()->Missed();
                    if (!visitorItem.GetIsFeature())
					{
						mScoreboard->AddMissed();
					}

                }
            }
        }
    }
}

/**
 * Opens the XML file and reads the nodes, creating items as appropriate.
 *
 * @param filename The filename of the file to load the level from.
 */
void Game::Load(const wxString &filename)
{
	mDisplayIntro = 40;
	mNumBugs = 0;
	mNumPrograms = 0;
    wxXmlDocument xmlDoc;
    if(!xmlDoc.Load(filename))
    {
        wxMessageBox(L"Unable to load XML file");
        return;
    }

    Clear();

    // Get the XML document root node
    auto root = xmlDoc.GetRoot();
	//cout<<(root->GetAttribute(L"level"))<<endl;
	mIntroText = root->GetAttribute(L"level");


    //
    // Traverse the children of the root
    // node of the XML document in memory!!!!
    //
    vector<shared_ptr<Program>> programVec;
    auto child = root->GetChildren();
    for( ; child; child=child->GetNext())
    {
        auto name = child->GetName();

		if ( name == L"program")
        {
            shared_ptr<Program> program = make_shared<Program>(this);
            programVec.push_back(program);
            program->XmlLoad(child);
            auto newChild = child->GetChildren();
            for( ; newChild; newChild=newChild->GetNext())
            {
                XmlItem(newChild, program);
            }
        }
    }

    // Need to ask more about how to do this in office hours

    for (auto item : programVec)
    {
        mItems.insert(begin(mItems), item);

		//adds to the program count for xml testing
		mNumPrograms+=1;
    }

    mScoreboard->Clear();
}

/**
 * Clear the game data.
 *
 * Deletes all known items in the game
 */
void Game::Clear()
{

    mItems.clear();

}

/**
 * Handle a node of type item.
 * @param node XML node
 * @param program (laptop)
 */
void Game::XmlItem(wxXmlNode *node, shared_ptr<Program> program)
{
    // A pointer for the item we are loading
    shared_ptr<Item> item;

    // We have an item. What type?
    auto type = node->GetAttribute(L"type");
    auto name = node->GetName();

    if (name == L"feature")
    {
        item = make_shared<FeatureBug>(this, program);

    }
    else if (name == L"bug")
    {
        if (type == L"null")
        {
            item = make_shared<NullBug>(this, program);
        }
        else if (type == L"redundancy")
        {
            item = make_shared<RedundancyBug>(this, program);
        }
        else if (type == L"garbage")
        {
            item = make_shared<GarbageBug>(this, program);
        }
        else if (type == L"")
        {
            item = make_shared<FeatureBug>(this, program);
        }
    }

	//adds to the bug count for xml testing
	mNumBugs+=1;
    item->XmlLoad(node);
    mItems.push_back(item);
}
/**
 * Adds item to game
 * @param item item to be added to game
 */
void Game::AddItem(shared_ptr<Item> item)
{
	mItems.push_back(item);
}



/**
 * Test an x,y click location to see if it clicked
 * on some item in the aquarium.
 * @param x X location in pixels
 * @param y Y location in pixels
 * @returns Pointer to item we clicked on or nullptr if none.
*/
std::shared_ptr<Item> Game::HitTest(int x, int y)
{
    double newX = (x - mXOffset) / mScale;
    double newY = (y - mYOffset) / mScale;

    for (auto i = mItems.rbegin(); i != mItems.rend();  i++)
    {
        if ((*i)->HitTest(newX, newY) && 0 < newY && newY < Height
                                            && 0 < newX && newX < Width)
        {
            return *i;
        }
    }

    if (0 < newX-EndPowerUpWidth && newX-EndPowerUpWidth < StartPowerUpWidth &&
        0 < newY-EndPowerUpHeight && newY-EndPowerUpHeight < StartPowerUpHeight
        && mScoreboard->GetFixed() >= PowerUpCost)
    {
        mScoreboard->SubPowerUpFixed();
        IsItemVisitor visitor;
        vector<double> Speeds;
        for (auto mItem : mItems)
        {
            mItem->Accept(&visitor);
            if (visitor.GetIsBug())
            {
                Speeds.push_back(visitor.GetBug()->GetSpeed());
                visitor.GetBug()->SetSpeed(0);
            }
        }

		mPowerUp = PowerUpTime;

        for (int i = 0; i < Speeds.size(); ++i)
        {
            mItems[i]->Accept(&visitor);
            if (visitor.GetIsBug())
            {
                visitor.GetBug()->SetSpeed(Speeds[i]);
            }
        }
    }

    return  nullptr;
}

/**
 * Accept a visitor for the collection
 * @param visitor The visitor for the collection
 */
void Game::Accept(ItemVisitor* visitor)
{
    for (auto tile : mItems)
    {
        tile->Accept(visitor);
    }
}

void Game::OnLeftDown(std::shared_ptr<Item> item)
{
    for (auto element : mItems)
    {
        if (item == element) {
            item->OnLeft();
            break;
        }
    }

}

void Game::SetLevelOver()
{

	for (auto x :  mItems )
	{
		if (!(x->NextLevelCheck()))
		{
			mOver = false;
			return;
		}
	}
	mOver = true;

}




/**
 * Returns Image from map of images based on name
 * @param ImageName name image of bug
 * @return pointer to wxImage file containing appropriate image
 */
std::shared_ptr<wxImage> Game::GetImageFromMap(std::wstring ImageName)
{
	//auto itr = mImageMap.find(ImageName);
	return mImageMap.at(ImageName);
}


