/**
 * @file Item.cpp
 * @author Connor Chapman, Colton Leslie
 */

#include "pch.h"
#include "Item.h"
#include "Game.h"
#include "IsItemVisitor.h"
#include "IsFatBugVisitor.h"

using namespace std;

/// Scale for the bigger FatBugs
const double FatBugScale = 1.25;

/// Directory for all images
const std::wstring ImagePath = L"images/";

/// Height of all sprites
const int SpriteHeight = 100.0;

/// Area around the item that is consider hitting it
const int BugHitRange = 50;

/**
 * Constructor
 * @param game The this item is a member of
 * @param filename filename.
 * @param numOfSheets number of sprite sheets
 */
Item::Item(Game *game, const std::wstring &filename, const int numOfSheets) : mGame(game), mNumOfSheets(numOfSheets)
{
    //vector<auto> {bitmap1,image1,bitmap2,image2}

	mItemImage = game->GetImageFromMap(filename);
}

/**
 * Destructor
 */
Item::~Item()
{

}

/**
 * Draw the view on a graphics context
 * @param graphics Graphics context to draw on
 */
void Item::Draw(std::shared_ptr<wxGraphicsContext> graphics)
{
    if(mItemBitmap.IsNull())
    {
        mItemBitmap = graphics->CreateBitmapFromImage(*mItemImage);
    }

	mItemWidth = mItemImage->GetWidth();
	mItemHeight = mItemImage->GetHeight()/mNumOfSheets;


    auto currentBitmap = graphics->CreateSubBitmap(mItemBitmap,0,mSheetHeight,mItemWidth,mItemHeight);

    IsItemVisitor visitor;
    IsFatBugVisitor fatBugVisitor;
    this->Accept(&visitor);
    this->Accept(&fatBugVisitor);

    if (fatBugVisitor.GetIsFatBug())
    {
        if (fatBugVisitor.GetBug()->GetSplat())
        {
            graphics->DrawBitmap(currentBitmap, -mItemWidth/2 , -mItemHeight/2 ,  mItemWidth, mItemHeight);
        }
        else
        {
            graphics->DrawBitmap(currentBitmap, -mItemWidth/2 , -mItemHeight/2 ,  mItemWidth*FatBugScale, mItemHeight*FatBugScale);
        }
    }
    else if (visitor.GetIsBug())
    {
        graphics->DrawBitmap(currentBitmap, -mItemWidth/2 , -mItemHeight/2 ,  mItemWidth, mItemHeight);
    }
    else
    {
        graphics->DrawBitmap(currentBitmap, mX-mItemWidth/2 , mY-mItemHeight/2 ,  mItemWidth, mItemHeight);
    }
}

 bool Item::NextLevelCheck ()
{
	IsItemVisitor visitor;
	this->Accept(&visitor);


	if (visitor.GetIsBug())
	{

		if (visitor.GetIsSplat()||visitor.GetIsMissed())
		{
			return true;
		}

		return false;

	}
	return true;

}

/**
 * Shifts sprite sheet
 */
void Item::UpdateSpriteSheet()
{
	mSheetHeight += SpriteHeight;
	if (mSheetHeight >= mNumOfSheets*SpriteHeight)
	{
		mSheetHeight = 0.0;
	}
}

/**
 * Updates item
 * @param elapsed Time since last call
 */
void Item::Update(double elapsed)
{
}

void Item::XmlLoad(wxXmlNode* node)
{
    long x, y;
    node->GetAttribute(L"x", L"0").ToLong(&x);
    node->GetAttribute(L"y", L"0").ToLong(&y);

    // Draw from the center of the bitmap
    mItemWidth = mItemImage->GetWidth();
    mItemHeight = mItemImage->GetHeight()/mNumOfSheets;

    mX = (int)x;
    mY = (int)y;
}

/**
 * Test to see if we hit this object with a mouse.
 * @param x X position to test
 * @param y Y position to test
 * @return true if hit.
 */
bool Item::HitTest(int x, int y)
{
    double dx = x - GetX();
    double dy = y - GetY();

    return sqrt(dx*dx + dy*dy) < BugHitRange;
}

void Item::SetImage(std::wstring newImage)
{
    mItemImage = std::make_shared<wxImage>(ImagePath+newImage, wxBITMAP_TYPE_ANY);
    mItemBitmap.UnRef();
    mNumOfSheets = 1;
    mSheetHeight = 0;
}

