/**
 * @file Game.h
 * @author Colton Leslie
 *
 * A class that implements a simple game that we can manipulate
 */

#ifndef PROJECT1_GAME_H
#define PROJECT1_GAME_H


#include <memory>
#include <wx/graphics.h>
#include "Item.h"
#include "Program.h"
#include <memory>
#include <wx/dcbuffer.h>
#include "Scoreboard.h"
#include <map>


/**
 *  Implements a simple game that we can manipulate
 */
class Game {
private:
    /// The amount of time that the text is displayed
	int mDisplayIntro = 160;
	/// The amount of powerup
	int mPowerUp = 0;

	/// All of the items in the Game
	std::vector<std::shared_ptr<Item>> mItems;

	/// State of screen being shrinked or not
	bool mShrinked;

	/// Scale for game
	double mScale;
    /// X offset for game
	double mXOffset;
	/// Y offset for game
    double mYOffset;

	/// number of bugs in the game
	int mNumBugs = 0;
	/// number of programs in the game
	int mNumPrograms = 0;

	/// if lvl is done.
	bool mOver = false;

	/// current level xml file name we are.
	std::wstring mLevel =  L"Data/level1.xml";
    ///scoreboard to display
	std::wstring mIntroText =  L"Level Zero";

    /// Scoreboard associated with this game
    std::unique_ptr<Scoreboard> mScoreboard;

    void XmlItem(wxXmlNode *node, std::shared_ptr<Program> program);

    /// map of all images for bugs.
	std::map<std::wstring, std::shared_ptr<wxImage>> mImageMap;

public:
    Game();

	void OnDraw(const std::shared_ptr<wxGraphicsContext>& graphics, int width, int height);
	void Update(double elapsed);
    void Load(const wxString &filename);
    void Clear();
	/**
 	 * GetShrink getter for game
     * @return mShrinked bool if shrinked or not
     */
    bool GetShrink() { return mShrinked; }
	/**
 	 * SetShrink setter for game
     * @param newVal bool
     */
	void SetShrink(bool newVal) { mShrinked = newVal; }
    std::shared_ptr<Item> HitTest(int x, int y);
    void Accept(ItemVisitor* visitor);
	/**
 	 * OnleftDown (mouse click command)
     * @param item shared ptr to an item
     */
    void OnLeftDown(std::shared_ptr<Item> item);

    /**
     * Adds 1 to mFixed in mScoreboard
     */
    void AddScoreFixed() { mScoreboard->AddFixed(); }

    /**
     * Adds 1 to mOops in mScoreboard
     */
    void AddScoreOops() { mScoreboard->AddOops(); }

    /**
     * Adds 1 to mMissed in mScoreboard
     */
    void AddScoreMissed() { mScoreboard->AddMissed(); }

	/**
 	 * GetNumBugs getter for game
     * @return number of bugs in the game
     */
	int GetNumBugs(){return mNumBugs;}
	/**
 	 * GetNumProgram getter for game
     * @return number of programs in the game
     */
	int GetNumPrograms(){return mNumPrograms;}

	/**
     * Sets mOver to true if level is over
     */
	void SetLevelOver();


	void AddItem(std::shared_ptr<Item> item);

	/**
	 * Adds 1 mNumBugs
	 */
	void IncrementBugCounter() { mNumBugs++; }

    /// Sets level to zero
	void SetLevelZero(){mLevel = L"Data/level0.xml";}

    /// Sets level to one
	void SetLevelOne(){mLevel = L"Data/level1.xml";}

    /// Sets level to two
	void SetLevelTwo(){mLevel = L"Data/level2.xml";}

    /// Sets level to three
	void SetLevelThree(){mLevel = L"Data/level3.xml";}


	std::shared_ptr<wxImage> GetImageFromMap(std::wstring ImageName);
};


#endif //PROJECT1_GAME_H
