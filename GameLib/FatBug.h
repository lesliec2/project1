/**
 * @file FatBug.h
 * @author Colton Leslie, John Schafer, Arul Srivastava
 */

#ifndef PROJECT1_FATBUG_H
#define PROJECT1_FATBUG_H

#include "Bug.h"
#include "Game.h"
#include "Program.h"
#include "FatBugIDE.h"

/**
 * A class that describes a fat bug
 */
class FatBug : public Bug{
private:
    /// Code that the user will have to fix
    std::wstring mCode;

    /// Correct code that user will enter
    std::wstring  mPassingCode;

    /// If this bug is in FatBug form
    bool mFat = false;

    /// Dialog box this FatBug is associated with
    FatBugIDE *mIDE = nullptr;

public:
    FatBug(Game *game, std::wstring SpriteImageName, int NumSpriteImages, std::shared_ptr<Program> program);

    /// Default constructor (disabled)
    FatBug() = delete;

    /// Copy constructor (disabled)
    FatBug(const FatBug &) = delete;

    /// Assignment operator (disabled)
    void operator=(const FatBug &) = delete;

    /**
     * Checks if the bug is fat
     * @return true or false depending on if the bug is fat or not
     */
    bool GetFat() { return mFat; }

    /**
     * Gets a pointer to the FatBugIDE
     * @return a pointer to the FatBugIDE
     */
    FatBugIDE* GetIDE() { return mIDE; }
    /**
     * Gets the code stored
     * @return a wstring containing the code
     */
    std::wstring GetCode() { return mCode; }

    /**
     * Gets the passing code
     * @return a wstring containing the passing code
     */
    std::wstring GetPassingCode() { return mPassingCode; }

    /**
     * Virtual void XmlLoad in item
     * @param node xml node
     */
    void XmlLoad(wxXmlNode* node) override;
};


#endif //PROJECT1_FATBUG_H
