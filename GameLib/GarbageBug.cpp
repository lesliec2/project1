/**
 * @file GarbageBug.cpp
 * @author Seth Darling
 */

#include "pch.h"
#include "GarbageBug.h"

using namespace std;

/// The bug sprite image
const std::wstring GarbageBugSpriteImageName = L"blue-maize-bug.png";

/// The splat image
const std::wstring BugSplatImageName = L"blue-maize-splat.png";

/// Number of sprite images
const int GarbageBugNumSpriteImages = 6;

/// Image path wstring for all images
const wstring ImagePath = L"images/";

/**
 * Constructor
 * @param game Game this bug is a member of
 * @param program that bug crawls to
 */

GarbageBug::GarbageBug(Game *game, std::shared_ptr<Program> program) : FatBug(game, GarbageBugSpriteImageName, GarbageBugNumSpriteImages, program)
{
}

/**
 * On a left click mouse event
 */
void GarbageBug::OnLeft()
{
    if (!Bug::GetMissed())
    {
        Bug::OnLeft();
        Item::SetImage(BugSplatImageName);
    }
}