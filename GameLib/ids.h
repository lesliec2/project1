/**
 * @file ids.h
 * @author Colton Leslie
 *
 * ID values for menus and other controls
 */

#ifndef PROJECT1_IDS_H
#define PROJECT1_IDS_H

#include <wx/defs.h>

/**
 * Menu id values
 */
enum IDs {
    /// Level>Level 0 menu option
    IDM_LEVEL0 = wxID_HIGHEST + 1,

    /// Level>Level 1 menu option
    IDM_LEVEL1,

    /// Level>Level 2 menu option
    IDM_LEVEL2,

    /// Level>Level 3 menu option
    IDM_LEVEL3,

    /// View>Shrink menu option
    IDM_SHRINK,

	/// FatBugIDE id
	IDM_FATBUGIDE
};
#endif //PROJECT1_IDS_H
