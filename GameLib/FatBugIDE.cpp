/**

 * @file FatBugIDE.cpp
 * @author seth, Colton Leslie
 */

#include "pch.h"
#include <wx/regex.h>
#include "FatBugIDE.h"
#include "FatBug.h"

FatBugIDE::FatBugIDE(wxWindow *parent, std::wstring code, std::wstring passingCode) : wxDialog(parent, wxID_ANY, L"Bug Squash IDE", wxDefaultPosition, wxSize( 500,500 ), wxDEFAULT_DIALOG_STYLE)
{
    mCode = code;
    mCorrectCode = passingCode;

    this->SetSizeHints( wxDefaultSize, wxDefaultSize );
    wxBoxSizer* bSizer1;
    bSizer1 = new wxBoxSizer( wxVERTICAL );

    mEditor = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE);
    bSizer1->Add( mEditor, 1, wxALIGN_LEFT|wxALL|wxEXPAND, 5);
    mEditor->AppendText(mCode);

    mOk = new wxButton( this, wxID_ANY, wxT("OK"), wxDefaultPosition, wxDefaultSize, 0);
    bSizer1->Add( mOk, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5);

    this->SetSizer( bSizer1 );
    this->Layout();

    this->Centre( wxBOTH );

    mOk->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FatBugIDE::OnOk ), NULL, this);
}

void FatBugIDE::OnOk(wxCommandEvent& event)
{
    mCode = mEditor->GetValue();
	wxDialog::EndModal(wxID_OK);
    wxRegEx passingCode(mCorrectCode);
    mPass = passingCode.Matches(mCode);
}
