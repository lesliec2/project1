/**

 * @file FatBug.cpp
 * @author Colton Leslie, John Schafer, Arul Srivastava
 */

#include "pch.h"
#include "FatBug.h"
#include "FatBugIDE.h"
#include <wx/regex.h>
using namespace std;


/**
 * Constructor
 * @param game Game this bug is a member of
 * @param SpriteImageName name of sprite image
 * @param NumSpriteImages Number of sprite images
 * @param program shared ptr to a program
 */
FatBug::FatBug(Game *game, std::wstring SpriteImageName, int NumSpriteImages, std::shared_ptr<Program> program) : Bug(game, SpriteImageName, NumSpriteImages, program)
{

    if (!mCode.empty())
    {
        mFat = true;
    }
}

void FatBug::XmlLoad(wxXmlNode *node)
{
    Bug::XmlLoad(node);
    auto x = node->GetChildren();
    if (x != nullptr)
    {
        mPassingCode = x->GetAttribute(L"pass");
        mCode = x->GetNodeContent();
        mFat = true;
    }
}
