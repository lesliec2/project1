/**
 * @file RedundancyBug.h
 * @author Colton Leslie
 *
 * Class that defines the Redundancy bug
 */

#ifndef PROJECT1_REDUNDANCYBUG_H
#define PROJECT1_REDUNDANCYBUG_H

#include "Bug.h"
#include "ItemVisitor.h"
#include "Game.h"

/**
 * A Redundancy Bug
 */
class RedundancyBug : public Bug{
private:

    /// Base image
    std::shared_ptr<wxImage> mImageBase;
	/// left wing image
	std::shared_ptr<wxImage> mImageLeftWing;
	/// right wing image
	std::shared_ptr<wxImage> mImageRightWing;
    /// top image
	std::shared_ptr<wxImage> mImageTop;

    /// Base bitmap
    wxGraphicsBitmap mBaseBitmap;
	/// left wing bitmap
    wxGraphicsBitmap mLeftWingBitmap;
	/// right wing bitmap
	wxGraphicsBitmap mRightWingBitmap;
    /// top bitmap
	wxGraphicsBitmap mTopBitmap;

	/// If bug is child of redundancy bug
	bool mIsChild = false;

	/// Seed for random placement of redundancy bugs
	int mSeed = 1;

	/// Amount to rotate wings
	double mWingRotation = -.1;

	/// If wings are moving up or down
	bool mWingsUp = false;

public:
    /// Default constructor (disabled)
    RedundancyBug() = delete;

    /// Copy constructor (disabled)
    RedundancyBug(const RedundancyBug &) = delete;

    /// Assignment operator
    void operator=(const RedundancyBug &) = delete;

    RedundancyBug(Game *Game, std::shared_ptr<Program> program, bool isChild=false);
	/**
 	 * Draw function for redundancy bug
 	 * @param graphics graphics for bug
     */
	void Draw (std::shared_ptr<wxGraphicsContext> graphics) override;

    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     */
    void Accept(ItemVisitor* visitor) override { visitor->VisitRedundancy(this); }

    /**
     * On a left click mouse event
     */
    virtual void OnLeft() override;
	/**
	  * Update function overide for redundancy bug
	  * @param elapsed double for elapsed time
	  */
    virtual void Update(double elapsed) override;

	/**
	 * Getter for mIsChild
	 * @return true if redundancy bug is a child
	 */
	bool GetIsChild() { return mIsChild; }
};


#endif //PROJECT1_REDUNDANCYBUG_H
