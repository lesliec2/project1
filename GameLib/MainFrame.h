/**
 * @file MainFrame.h
 * @author Arul Srivastava
 *
 * The top-level (main) frame of the application
 */

#ifndef PROJECT1_MAINFRAME_H
#define PROJECT1_MAINFRAME_H

#include <wx/filehistory.h>
#include <wx/config.h>



class GameView;

/**
 * Main frame for the Game
 */
class MainFrame : public wxFrame
{
private:
    /// View class for our city
    GameView *mGameView;

	void OnExit(wxCommandEvent& event);
	/**
	 * On about function for the Game
	 * @param event WxCommandEvent
	 */
	void OnAbout(wxCommandEvent& event);

    void OnClose(wxCloseEvent &event);

    /// Application associated configurations
    wxConfig mConfig;

    /// File history
    wxFileHistory mHistory;
public:
    void Initialize();
};


#endif //PROJECT1_MAINFRAME_H
