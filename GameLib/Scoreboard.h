/**
 * @file Scoreboard.h
 * @author John Schafer
 *
 *
 */

#ifndef PROJECT1_GAMELIB_SCOREBOARD_H
#define PROJECT1_GAMELIB_SCOREBOARD_H

#include "Item.h"

/**
 * Class to display the scoreboard with game statistics.
 */
class Scoreboard{
private:
	/// Count for Fixed bugs
	int mFixed = 0;

    /// Count for missed bugs
    int mMissed = 0;

    /// Count for feature bugs mistakenly clicked
    int mOops = 0;
public:
    /// Default constructor (disabled)
    Scoreboard() = delete;

    /// Copy constructor (disabled)
    Scoreboard(const Scoreboard &) = delete;

    /// Assignment operator
    void operator=(const Scoreboard &) = delete;

	Scoreboard(Game *game);

	void Draw (std::shared_ptr<wxGraphicsContext> graphics);
	/**
	 * Clears scoreboard
	 */
    void Clear();

    /**
     * Adds one to mFixed
     */
    void AddFixed() { mFixed++; }

    /**
     * Adds one to mMissed
     */
    void AddMissed() { mMissed++; }

    /**
     * Subtracts 7 from mFixed because you used the powerup.
     */
    void SubPowerUpFixed() { mFixed -= 7; }

    /**
     * Adds one to mOops
     */
    void AddOops() { mOops++; }

    /**
     * Getter for mFixed
     * @return value of mFixed
     */
    int GetFixed() { return mFixed; }

    /**
     * Getter for mMissed
     * @return value of mMissed
     */
    int GetMissed() { return mMissed; }

    /**
     * Getter for mOops
     * @return value of mOops
     */
    int GetOops() { return mOops; }
};

#endif //PROJECT1_GAMELIB_SCOREBOARD_H
