/**
 * @file ItemVisitor.h
 * @author Colton Leslie
 */

#ifndef PROJECT1_ITEMVISITOR_H
#define PROJECT1_ITEMVISITOR_H

class Item;
class Bug;
class GarbageBug;
class FeatureBug;
class RedundancyBug;
class NullBug;
class Program;
class FatBug;

/**
 *
 * Item visitor base class
 */

class ItemVisitor {
protected:
    /**
     * Constructor
     * Ensures this is an abstract class
     */
    ItemVisitor() {}

public:
    virtual ~ItemVisitor() {}

    /**
     * Visit a garbagebug object
     * @param garbageBug Item we are visiting
     */
    virtual void VisitGarbage(GarbageBug* garbageBug) {}

    /**
     * Visit a nullbug object
     * @param nullBug Item we are visiting
     */
    virtual void VisitNull(NullBug* nullBug) {}

    /**
     * Visit a redudancy bug object
     * @param redunBug Item we are visiting
     */
    virtual void VisitRedundancy(RedundancyBug* redunBug) {}

    /**
     * Visit a featurebug file
     * @param featBug Item object we are visiting
     */
    virtual void VisitFeature(FeatureBug* featBug) {}

    /**
     * Visit a program file
     * @param program Item object we are visiting
     */
    virtual void VisitProgram(Program* program) {}
};


#endif //PROJECT1_ITEMVISITOR_H
