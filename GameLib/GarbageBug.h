/**
 * @file GarbageBug.h
 * @author Seth Darling
 *
 *
 */

#ifndef PROJECT1_GAMELIB_GARBAGEBUG_H
#define PROJECT1_GAMELIB_GARBAGEBUG_H

#include "Bug.h"
#include "FatBug.h"
#include "Game.h"
#include "ItemVisitor.h"

/**
 * A Garbage Bug
 */
class GarbageBug : public FatBug
{
private:

public:
	/// Default constructor (disabled)
	GarbageBug() = delete;

	/// Copy constructor (disabled)
	GarbageBug(const GarbageBug &) = delete;

	/// Assignment operator (disabled)
	void operator=(const GarbageBug &) = delete;

	GarbageBug(Game *Game, std::shared_ptr<Program> program);

    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     */
    void Accept(ItemVisitor* visitor) override { visitor->VisitGarbage(this); }

    /**
     * On a left click mouse event
     */
    virtual void OnLeft() override;
};

#endif //PROJECT1_GAMELIB_GARBAGEBUG_H
