/**
 * @file ProgramAssociationVisitor.cpp
 * @author Colton Leslie
 */

#include "pch.h"
#include "ProgramAssociationVisitor.h"

/**
 * Visit a GarbageBug object
 * @param garbageBug GarbageBug item we are visiting
 */
void ProgramAssociationVisitor::VisitGarbage(GarbageBug *garbageBug)
{
    mProgram = garbageBug->GetProgram();
}

/**
 * Visit a NullBug object
 * @param nullBug NullBug item we are visiting
 */
void ProgramAssociationVisitor::VisitNull(NullBug *nullBug)
{
    mProgram = nullBug->GetProgram();
}

/**
 * Visit a FeatureBug object
 * @param featBug FeatureBug item we are visiting
 */
void ProgramAssociationVisitor::VisitFeature(FeatureBug *featBug)
{
    mProgram = featBug->GetProgram();
}

/**
 * Visit a RedundancyBug object
 * @param redunBug RedundancyBug item we are visiting
 */
void ProgramAssociationVisitor::VisitRedundancy(RedundancyBug *redunBug)
{
    mProgram = redunBug->GetProgram();
}