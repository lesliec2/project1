/**
 * @file FeatureBug.h
 * @author Arul Srivastava
 */

#ifndef PROJECT1_FEATUREBUG_H
#define PROJECT1_FEATUREBUG_H

#include "Bug.h"
#include "Game.h"
#include "ItemVisitor.h"

/**
 * A Feature Bug
 */
class FeatureBug : public Bug
{
private:

public:
    /// Default constructor (disabled)
    FeatureBug() = delete;

    /// Copy constructor (disabled)
    FeatureBug(const FeatureBug &) = delete;

    /// Assignment operator (disabled)
    void operator=(const FeatureBug &) = delete;

    FeatureBug(Game *Game, std::shared_ptr<Program> program);

    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     */
    void Accept(ItemVisitor* visitor) override { visitor->VisitFeature(this); }

    /**
     * On a left click mouse event
     */
    virtual void OnLeft() override;
};


#endif //PROJECT1_FEATUREBUG_H
