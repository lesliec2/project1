/**
 * @file Program.h
 * @author Connor Chapman
 *
 *
 */

#ifndef PROJECT1_GAMELIB_PROGRAM_H
#define PROJECT1_GAMELIB_PROGRAM_H

#include "Item.h"
#include "ItemVisitor.h"

/**
 * Program Class
 */
class Program : public Item
{
private:
	/// Time since last sprite sheet change
	double mSpriteSheetTime = 0;

    /// Name of this program
    std::wstring mName;

public:
    /// Default constructor (disabled)
    Program() = delete;

    /// Copy constructor (disabled)
    Program(const Program &) = delete;

    /// Assignment operator
    void operator=(const Program &) = delete;
    Program(Game *game);

	void Update(double elapsed) override;
	/**
	 * Draw override for the program item
	 * @param graphics graphics context
	 */
    void Draw(std::shared_ptr<wxGraphicsContext> graphics) override;

	/**
	 * Loads an xml load
	 * @param node wxXmlNode
	 */
    void XmlLoad(wxXmlNode* node) override;

    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     */
    void Accept(ItemVisitor* visitor) override { visitor->VisitProgram(this); }

    /**
     * On a left click mouse event
     */
    virtual void OnLeft() override {};
};

#endif //PROJECT1_GAMELIB_PROGRAM_H
