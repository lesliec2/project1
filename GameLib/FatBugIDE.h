/**
 * @file FatBugIDE.h
 * @author Seth and Arul
 */

#ifndef PROJECT1_GAMELIB_FATBUGIDE_H
#define PROJECT1_GAMELIB_FATBUGIDE_H

#include <wx/dialog.h>
#include <wx/window.h>
#include <wx/textctrl.h>
class FatBug;
#include "ids.h"
#include <memory>

/**
 * class that creates coding IDE for users to fix
 */
class FatBugIDE : public wxDialog
{
private:
    /// Code displayed in window
    std::wstring mCode;

    /// What the code needs to be changed to
    std::wstring mCorrectCode;

    /// The text editor that we are using in the dialog box
    wxTextCtrl* mEditor;

    /// The button we are going to use
    wxButton* mOk;

    /// Pointer to FatBug associated with window
    FatBug *mFatBug = nullptr;

    /// Whether the users answer was passed or not
    bool mPass = false;

public:
	/**
 	 * FatBugIDE window for fatbug.
 	 * @param parent window
 	 * @param code code user enters
 	 * @param passingCode code to pass
 	 */
    FatBugIDE(wxWindow *parent, std::wstring code, std::wstring passingCode);
	/**
 	 * OnOk handler
 	 * @param event command event
 	 */
    void OnOk(wxCommandEvent& event);
	/**
 	 * GetPass
 	 * @return mPass if Code Passes
 	 */
    bool GetPass() { return mPass; }

};

#endif //PROJECT1_GAMELIB_FATBUGIDE_H
