/**
 * @author Colton Leslie
 * @file NullBug.cpp
 */

#include "pch.h"
#include "NullBug.h"

/// The bug sprite image
const std::wstring NullBugSpriteImageName = L"scarlet-gray-bug.png";

/// The splat image
const std::wstring BugSplatImageName = L"scarlet-gray-splat.png";

/// Number of sprite images
const int NullBugNumSpriteImages = 6;

/**
 * Constructor
 * @param game Game this bug is a member of
 * @param program program this bug crawls towards
 */
NullBug::NullBug(Game *game, std::shared_ptr<Program> program) : FatBug(game, NullBugSpriteImageName, NullBugNumSpriteImages, program)
{
}

/**
 * On a left click mouse event
 */
void NullBug::OnLeft()
{
    if (!Bug::GetMissed())
    {
        Bug::OnLeft();
        Item::SetImage(BugSplatImageName);
    }
}

