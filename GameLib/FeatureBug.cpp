/**
 * @file FeatureBug.cpp
 * @author Arul Srivastava
 */

#include "pch.h"
#include "FeatureBug.h"

using namespace std;

/// The feature sprite image
const std::wstring FeatureBugSpriteImageName = L"feature.png";

/// The splat image
const std::wstring BugSplatImageName = L"feature-splat.png";

/// Number of sprite images
const int FeatureBugNumSpriteImages = 7;

/// Image path wstring for all images
const wstring ImagePath = L"images/";

/**
 * Constructor
 * @param game Game this bug is a member of
 * @param program program the bugs head towards
 */
FeatureBug::FeatureBug(Game *game, std::shared_ptr<Program> program) : Bug(game, FeatureBugSpriteImageName, FeatureBugNumSpriteImages, program)
{
}

/**
 * On a left click mouse event
 */
void FeatureBug::OnLeft()
{
    if (!Bug::GetMissed())
    {
        Bug::OnLeft();
        Item::SetImage(BugSplatImageName);
    }
}
