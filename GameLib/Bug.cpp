/**
 * @file Bug.cpp
 * @author seth
 */

#include "pch.h"
#include "Bug.h"
#include "Item.h"
#include "IsItemVisitor.h"

using namespace std;

/// Height of all sprites
const int SpriteHeight = 100.0;

/// Random seed for bug
const int seedValue = 12020;






/**
 * Constructor
 * @param game The game we are in
 * @param filename Filename for the image we use
 * @param numOfSheets Number of sprite sheets
 * @param program shared ptr to a program.
 */
Bug::Bug(Game *game, std::wstring filename, int numOfSheets, std::shared_ptr<Program> program) :
	Item(game, filename, numOfSheets)
{
	mSpeed = 0;
	mRotation = 0;
	SetLocation(150, 150);
    mProgram = program;
    mStart = 0;
	srand(seedValue);

}

/**
 * Updates bug
 * @param elapsed Time since last call
 */
void Bug::Update(double elapsed)
{
    mStart = mStart - elapsed;
    if (mStart <= 0 && !mSplat)
    {
        Item::Update(elapsed);


		mRotation = atan2(((mProgram->GetY()-GetY())),((mProgram->GetX())-GetX()));

        double newXInterval = elapsed * mSpeed * cos(mRotation);
        double newYInterval = elapsed * mSpeed * sin(mRotation);

        SetLocation(GetX() + newXInterval, GetY() + newYInterval);

		mSpriteSheetTime += elapsed;
        if (mSpriteSheetTime > (3.0 / mSpeed))
        {
            mSpriteSheetTime = 0;
            UpdateSpriteSheet();
        }
    }
}

void Bug::Draw(std::shared_ptr<wxGraphicsContext> graphics)
{
    if (mSplat)
    {
        graphics->PushState();
        graphics->Translate(GetX(), GetY());
        Item::Draw(graphics);
        graphics->PopState();
    }
    else if (!mMissed)
    {
        graphics->PushState();
        graphics->Translate(GetX(), GetY());
        graphics->Rotate(mRotation);
        Item::Draw(graphics);
        graphics->PopState();
    }
}

void Bug::XmlLoad(wxXmlNode *node)
{
    Item::XmlLoad(node);

    long start, speed;

    node->GetAttribute(L"start", L"0").ToLong(&start);
    node->GetAttribute(L"speed", L"20").ToLong(&speed);

    mStart = (double) start;
    mSpeed = (double) speed;
    mRotation = 0;
}

void Bug::OnLeft()
{

    mSplat = true;
}


void Bug::Missed()
{
    mMissed = true;
}


