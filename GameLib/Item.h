/**
 * @file Item.h
 * @author Connor Chapman, Colton Leslie
 *
 * Base class for any item in our application
 */

#ifndef PROJECT1_ITEM_H
#define PROJECT1_ITEM_H

#include "ItemVisitor.h"

class Game;

/**
 * Base class for any item in our Game
 */
class Item {
protected:
    Item(Game *game, const std::wstring &filename, const int numOfSheets);

private:
    /// The Game this item is contained in
    Game *mGame;

    /// Item location in the game
    double mX = 0;     ///< X location for the center of the item
    double mY = 0;     ///< Y location for the center of the item

    /// The underlying item image
    std::shared_ptr<wxImage> mItemImage;

    /// The bitmap we can display for this item
    wxGraphicsBitmap mItemBitmap;

    /// Width of the imgae/bitmap
    double mItemWidth;

    /// Height of the image/bitmap
    double mItemHeight;

    /// Item Sprite Sheet info
    int mNumOfSheets = 0;

    /// Sprite sheet height
    double mSheetHeight = 0.0;

public:

    /// Default constructor (disabled)
    Item() = delete;

    /// Copy constructor (disabled)
    Item(const Item &) = delete;

    /// Assignment operator
    void operator=(const Item &) = delete;

    virtual ~Item();

    virtual void Draw (std::shared_ptr<wxGraphicsContext> graphics);

	/**
	 * helps to check if next level should be loaded with help of visitor class
     * @return bool if item is a program or a missed or splatted bug
     */
	 bool NextLevelCheck();

    /**
     * Getter for the x position of this item
     * @return Value of mX
     */
    double GetX() { return mX; }

    /**
     * Getter for the y position of this item
     * @return Value of mY
     */
    double GetY() { return mY; }

	/**
 	 * Set the item location
 	 * @param x X location in pixels
 	 * @param y Y location in pixels
 	 */
	void SetLocation(double x, double y) { mX = x; mY = y; }

	/**
 	 * Getter for the Number of sheets on sprite sheet
 	 * @return Number of sheets
 	 */
	int GetSheetNumber() { return mNumOfSheets; }

	/**
  	 * Getter for the sheet height of item
  	 * @return Height of Sheet
  	 */
	double GetSheetHeight() { return mSheetHeight; }

	/**
   	 * Getter for the item this item has
   	 * @return Pointer to image
   	 */
	std::shared_ptr<wxImage> GetImage() { return mItemImage; }

	/**
 	 * GetWidth getter for item
     * @return mItemWidth double
     */
	double GetWidth() { return mItemWidth; }

	/**
 	 * GetHeight getter for item
     * @return mItemHeight double
     */
    double GetHeight() { return mItemHeight; }

	virtual void Update(double elapsed);

	void UpdateSpriteSheet();

	/**
 	 * Virtual void XmlLoad in item
     * @param node xml node
     */
    virtual void XmlLoad(wxXmlNode* node);

	/**
  	 * Set image function for item
     * @param newImage wstring of image name
     */
    void SetImage(std::wstring newImage);

    bool HitTest(int x, int y);

    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     */
    virtual void Accept(ItemVisitor* visitor) = 0;

    /**
     * On a left click mouse event
     */
    virtual void OnLeft() = 0;

	/**
 	 * GetGame getter for game
     * @return pointer to game item is in
     */
	Game* GetGame() { return mGame; }

};


#endif //PROJECT1_ITEM_H
