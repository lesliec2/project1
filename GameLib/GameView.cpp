/**
 * @file GameView.cpp
 * @author Colton Leslie
 * @author Connor Chapman
 */

#include "pch.h"
#include "GameView.h"
#include "ids.h"
#include "NullBug.h"
#include "RedundancyBug.h"
#include "GarbageBug.h"
#include "IsItemVisitor.h"
#include "IsFatBugVisitor.h"
#include "FatBugIDE.h"

#include <wx/dcbuffer.h>

using namespace std;

/// Path for level zero
const wstring Level0 = L"Data/level0.xml";

/// Path for level one
const wstring Level1 = L"Data/level1.xml";

/// Path for level two
const wstring Level2 = L"Data/level2.xml";

/// Path for level three
const wstring Level3 = L"Data/level3.xml";

/// Frame duration in milliseconds
const int FrameDuration = 30;

/**
 * Initialize the game view class.
 * @param mainFrame The parent window for this class
 */
void GameView::Initialize(wxFrame* mainFrame)
{
	mTimer.SetOwner(this);
	mTimer.Start(FrameDuration);

	Create(mainFrame, wxID_ANY,
           wxDefaultPosition, wxDefaultSize,
           wxFULL_REPAINT_ON_RESIZE);

    SetBackgroundStyle(wxBG_STYLE_PAINT);

    Bind(wxEVT_PAINT, &GameView::OnPaint, this);
	Bind(wxEVT_TIMER, &GameView::OnTimer, this);
    Bind(wxEVT_LEFT_DOWN, &GameView::OnLeftDown, this);
	Bind(wxEVT_LEFT_DCLICK, &GameView::OnLeftDoubleClick, this);
    mainFrame->Bind(wxEVT_COMMAND_MENU_SELECTED, &GameView::OnLevel0, this, IDM_LEVEL0);
    mainFrame->Bind(wxEVT_COMMAND_MENU_SELECTED, &GameView::OnLevel1, this, IDM_LEVEL1);
    mainFrame->Bind(wxEVT_COMMAND_MENU_SELECTED, &GameView::OnLevel2, this, IDM_LEVEL2);
    mainFrame->Bind(wxEVT_COMMAND_MENU_SELECTED, &GameView::OnLevel3, this, IDM_LEVEL3);
    mainFrame->Bind(wxEVT_COMMAND_MENU_SELECTED, &GameView::OnShrink, this, IDM_SHRINK);
	mStopWatch.Start();
}


/**
 * Paint event, draws the window.
 * @param event Paint event object
 */
void GameView::OnPaint(wxPaintEvent &event)
{
	// Compute the time that has elapsed
	// since the last call to OnPaint.
	auto newTime = mStopWatch.Time();
	auto elapsed = (double)(newTime - mTime) * 0.001;
	mTime = newTime;
	mGame.Update(elapsed);

	// Create a double-buffered display context
    wxAutoBufferedPaintDC dc(this);

    // Clear the image to black
    wxBrush background(*wxBLACK);
    dc.SetBackground(background);
    dc.Clear();

    // Create a graphics context
    auto gc =
            std::shared_ptr<wxGraphicsContext>(wxGraphicsContext::Create(dc));

    // Tell the game class to draw
    wxRect rect = GetRect();
    mGame.OnDraw(gc, rect.GetWidth(), rect.GetHeight());
}

/**
* Handle the Timer Event
* @param event
*/
void GameView::OnTimer(wxTimerEvent &event)
{
	Refresh();
}

void GameView::OnShrink(wxCommandEvent &event)
{
    if (mGame.GetShrink())
    {
        mGame.SetShrink(false);
    }
    else
    {
        mGame.SetShrink(true);
    }
}

void GameView::OnLevel0(wxCommandEvent &event)
{
    wxString filename = Level0;

	mGame.SetLevelZero();
    mGame.Load(filename);
	Refresh();
}

void GameView::OnLevel1(wxCommandEvent &event)
{
    wxString filename = Level1;
	mGame.SetLevelOne();
    mGame.Load(filename);
    Refresh();
}

void GameView::OnLevel2(wxCommandEvent &event)
{
    wxString filename = Level2;
	mGame.SetLevelTwo();
    mGame.Load(filename);
    Refresh();
}

void GameView::OnLevel3(wxCommandEvent &event)
{
    wxString filename = Level3;
	mGame.SetLevelThree();
    mGame.Load(filename);
    Refresh();
}

void GameView::OnLeftDown(wxMouseEvent& event)
{
    mStopWatch.Pause();
    mGrabbedItem = mGame.HitTest(event.GetX(), event.GetY());
    if (mGrabbedItem != nullptr)
    {
        IsItemVisitor visitor;
        IsFatBugVisitor fatBugVisitor;

        mGrabbedItem->Accept(&fatBugVisitor);
        mGrabbedItem->Accept(&visitor);

        if (!fatBugVisitor.GetIsFatBug() && !visitor.GetIsProgram() && !visitor.GetBug()->GetMissed())
        {
            if (!visitor.GetBug()->GetSplat())
            {
                if (visitor.GetIsFeature())
                {
                    mGame.AddScoreOops();
                }
                else if (visitor.GetIsBug() && !visitor.GetIsFeature() && !visitor.GetIsParent())
                {
                    mGame.AddScoreFixed();
                }

                if (!visitor.GetIsProgram())
                {
                    mGame.OnLeftDown(mGrabbedItem);
                }
            }
        }
        Refresh();
    }
    mStopWatch.Resume();
}

void GameView::OnLeftDoubleClick(wxMouseEvent& event)
{
	mGrabbedItem = mGame.HitTest(event.GetX(), event.GetY());
	if (mGrabbedItem != nullptr)
	{
		IsFatBugVisitor visitor;
		mGrabbedItem->Accept(&visitor);

        if (visitor.GetIsFatBug())
        {
            mStopWatch.Pause();
            FatBugIDE fatBug(this, visitor.GetBug()->GetCode(), visitor.GetBug()->GetPassingCode());
            fatBug.ShowModal();
            if (fatBug.GetPass())
            {
                visitor.GetBug()->OnLeft();
            }
            mStopWatch.Resume();
        }

        Refresh();
	}
}