/**
 * @author Colton Leslie
 * @file NullBug.h
 *
 * Class that defines the null bug in our game
 */

#ifndef PROJECT1_NULLBUG_H
#define PROJECT1_NULLBUG_H

#include "Bug.h"
#include "ItemVisitor.h"
#include "FatBug.h"

/**
 * A Null Bug
 */
class NullBug : public FatBug{
private:

public:
    /// Default constructor (disabled)
    NullBug() = delete;

    /// Copy constructor (disabled)
    NullBug(const NullBug &) = delete;

    /// Assignment operator
    void operator=(const NullBug &) = delete;

    NullBug(Game *Game, std::shared_ptr<Program> program);

    /**
     * Accept a visitor
     * @param visitor The visitor we accept
     */
    void Accept(ItemVisitor* visitor) override { visitor->VisitNull(this); }


    /**
     * On a left click mouse event
     */
    virtual void OnLeft() override;
};


#endif //PROJECT1_NULLBUG_H
