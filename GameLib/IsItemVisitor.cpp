/**
 * @file IsItemVisitor.cpp
 * @author Colton Leslie
 */

#include "pch.h"
#include <iostream>
#include "IsItemVisitor.h"


/**
 * Visit a GarbageBug object
 * @param garbageBug GarbageBug item we are visiting
 */
void IsItemVisitor::VisitGarbage(GarbageBug *garbageBug)
{

    mIsBug = true;
    mIsFeature = false;
    mIsProgram = false;
	mIsGarbage = true;
    mBug = garbageBug;

	//std::cout<<"garb splt,miss"<<garbageBug->GetSplat()<<garbageBug->GetMissed()<<std::endl;
	mSplat = garbageBug->GetSplat();
	mMissed = garbageBug->GetMissed();
}

/**
 * Visit a NullBug object
 * @param nullBug NullBug item we are visiting
 */
void IsItemVisitor::VisitNull(NullBug *nullBug)
{
    mIsBug = true;
    mIsFeature = false;
    mIsProgram = false;
    mBug = nullBug;
	mSplat = nullBug->GetSplat();
	mMissed = nullBug->GetMissed();
}

/**
 * Visit a FeatureBug object
 * @param featBug FeatureBug item we are visiting
 */
void IsItemVisitor::VisitFeature(FeatureBug *featBug)
{
    mIsBug = true;
    mIsFeature = true;
    mIsProgram = false;
    mBug = featBug;
	mSplat = featBug->GetSplat();
	mMissed = featBug->GetMissed();
	//std::cout<<" feat splt,miss"<<featBug->GetSplat()<<featBug->GetMissed()<<std::endl;
}

/**
 * Visit a RedundancyBug object
 * @param redunBug RedundancyBug item we are visiting
 */
void IsItemVisitor::VisitRedundancy(RedundancyBug *redunBug)
{
    mIsBug = true;
    mIsFeature = false;
    mIsProgram = false;
    mBug = redunBug;
	mSplat = redunBug->GetSplat();
	mMissed = redunBug->GetMissed();
	mIsRedundancyBug = true;
	mIsParent = !redunBug->GetIsChild();

}

/**
 * Visit a Program object
 * @param program Program item we are visiting
 */
void IsItemVisitor::VisitProgram(Program *program)
{
    mIsBug = false;
    mIsFeature = false;
    mIsProgram = true;
    mBug = nullptr;
}