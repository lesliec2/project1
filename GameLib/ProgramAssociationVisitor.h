/**
 * @file ProgramAssociationVisitor.h
 * @author Colton Leslie
 *
 * Visitor class that will tell us the item it visits the program it
 * is associated with.
 */

#ifndef PROJECT1_PROGRAMASSOCIATIONVISITOR_H
#define PROJECT1_PROGRAMASSOCIATIONVISITOR_H

#include "ItemVisitor.h"
#include "GarbageBug.h"
#include "NullBug.h"
#include "RedundancyBug.h"
#include "FeatureBug.h"
#include "Program.h"

/**
 * Association visitor class
 */
class ProgramAssociationVisitor : public ItemVisitor {
private:
    /// If the item is a bug or not
    std::shared_ptr<Program> mProgram = nullptr;
public:

    void VisitGarbage(GarbageBug* garbageBug) override;

    void VisitNull(NullBug* nullBug) override;

    void VisitRedundancy(RedundancyBug* redunBug) override;

    void VisitFeature(FeatureBug* featBug) override;

    /**
     * Getter for mProgram
     * @return value of mProgram
     */
     std::shared_ptr<Program> GetProgram() { return mProgram; }
};


#endif //PROJECT1_PROGRAMASSOCIATIONVISITOR_H
