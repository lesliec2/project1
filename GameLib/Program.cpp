/**
 * @file Program.cpp
 * @author Connor Chapman
 */

#include "pch.h"
#include "Program.h"
#include "Item.h"

/// The font color to use
const wxColour FontColor = wxColour(225, 225, 225);

/// The laptop sprite image
const std::wstring SpriteImageName = L"laptop.png";

/// Number of sprite images/
const int NumSpriteImages = 1;

/// Image path wstring for all images
const std::wstring ImagePath = L"images/";

/// Program name font size
const int ProgramFontSize = 22;

/**
 * Constructor
 * @param game The game we are in
 */
Program::Program(Game *game) :
	Item(game, SpriteImageName, NumSpriteImages)
{
    mName = L"";
}

/**
 * Updates Program
 * @param elapsed Time since last call
 */
void Program::Update(double elapsed)
{
}

void Program::Draw(std::shared_ptr<wxGraphicsContext> graphics)
{
    Item::Draw(graphics);

    // Need to go to helproom about how to center text
    wxFont labelFont(wxSize(0, ProgramFontSize),
                     wxFONTFAMILY_SWISS,
                     wxFONTSTYLE_NORMAL,
                     wxFONTWEIGHT_BOLD);

    graphics->SetFont(labelFont, FontColor);
    // Calculate text dimensions
    wxString text = mName;
    wxDouble width, height, descent, externalLeading;
    graphics->GetTextExtent(text, &width, &height, &descent, &externalLeading);
    graphics->DrawText(text, GetX()-(width/2), GetY()-(height/2));

    // Draw text centered above program
//    auto x = GetX() + (GetWidth() / 2) - (textWidth / 2);
//    auto y = GetY() - (GetHeight() / 6);
//    graphics->DrawText(mName, x, y);
    //graphics->DrawText(mName, GetX()-(GetWidth()/3.5), GetY()-(GetHeight()/6));
}

void Program::XmlLoad(wxXmlNode *node)
{
    Item::XmlLoad(node);
    mName = node->GetAttribute(L"name");
}