/**
 * @file IsItemVisitor.h
 * @author Colton Leslie
 *
 */

#ifndef PROJECT1_ISITEMVISITOR_H
#define PROJECT1_ISITEMVISITOR_H


#include "ItemVisitor.h"
#include "Bug.h"
#include "GarbageBug.h"
#include "NullBug.h"
#include "RedundancyBug.h"
#include "FeatureBug.h"
#include "Program.h"
#include "FatBug.h"

/**
 * IsItem visitor class
 */
class IsItemVisitor : public ItemVisitor{
private:
    /// If the item is a bug or not
    bool mIsBug = false;

    /// If the item is a Feature Bug or not
    bool mIsFeature = false;

	/// if is garbage bug
	bool mIsGarbage = false;

    /// If the item is a Program
    bool mIsProgram = false;

    /// The Bug that has been visited
    Bug* mBug = nullptr;

	/// if bug is splat
	bool mSplat = false;
	/// if bug is missed
	bool mMissed = false;

	/// If the item is a redundancy bug
	bool mIsRedundancyBug = false;

	/// If the item is a parent
	bool mIsParent = false;


public:

    void VisitGarbage(GarbageBug* garbageBug) override;

    void VisitNull(NullBug* nullBug) override;

    void VisitRedundancy(RedundancyBug* redunBug) override;

    void VisitFeature(FeatureBug* featBug) override;

    void VisitProgram(Program* program) override;

    /**
     * Gets the value of mIsBug
     * @return value of mIshBug
     */
    bool GetIsBug() { return mIsBug; }

    /**
     * Gets the value of mIsBug
     * @return value of mIshBug
     */
    bool GetIsFeature() { return mIsFeature; }

    /**
     * Gets the value of mIsProgram
     * @return value of mIsProgram
     */
	bool GetIsProgram() { return mIsProgram; }

	/**
     * Gets the value of mBug
     * @return value of mBug
     */
    Bug* GetBug() { return mBug; }


	/**
	 * Gets value of mIsRedundancyBug
	 * @return If item is Redundancy Bug
	 */
	bool GetIsRedundancyBug() { return mIsRedundancyBug; }

	/**
	 * Returns whether or not item is a child
	 * @return If item is Redundancy Bug
	 */
	bool GetIsParent() { return mIsParent; }

	/**
	 * Getter to determine if a bug is splatted
	 * @return bool if splat
	 */
	bool GetIsSplat() { return mSplat; }
	/**
	 * Getter to determine if a bug is missed
	 * @return bool if missed
	 */

	bool GetIsMissed() { return mMissed; }
	/**
	 * Getter to determine if a bug is garbo
	 * @return bool if garbo
	 */
	bool GetIsGarbo() {return mIsGarbage;}

};


#endif //PROJECT1_ISITEMVISITOR_H
