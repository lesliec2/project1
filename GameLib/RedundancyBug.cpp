/**
 * @file RedundancyBug.cpp
 * @author Colton Leslie
 */

#include "pch.h"
#include "RedundancyBug.h"

using namespace std;

/// The bug base image
const std::wstring RedundancyFlyBase = L"redundancy-fly-base.png";

/// The bug top image
const std::wstring RedundancyFlyTop = L"redundancy-fly-top.png";

/// The left wing image
const std::wstring RedundancyFlyLeftWing = L"redundancy-fly-lwing.png";

/// The right wing image
const std::wstring RedundancyFlyRightWing = L"redundancy-fly-rwing.png";

/// The splat image
const std::wstring RedundancyFlySplat = L"redundancy-fly-splat.png";

/// Wing flapping period in seconds
const double WingPeriod = 0.2;

/// Starting rotation angle for wings in radians
const double WingRotateStart = 0.0;

/// End rotation angle for wings in radians
const double WingRotateEnd = 1.5;

/// How many sets of wings does this bug have?
const int NumberOfSetsOfWings = 4;

/// Number of virtual pixels between each of the wing sets
const int WingSetXOffset = 12;

/// X position relative to center of bug for the first (back) wing set
const int FirstWingSetX = -36;

/// Y position relative to center of bug for the right wings. The negative
/// of this is the Y position for the left wings.
const int WingSetY = 5;

/// The number of sprite images
const int RedundancyBugNumSpriteImages = 1;

/// Image path wstring for all images
const wstring ImagePath = L"images/";

/// Distance to spawn new redundancy bugs
double SpawnDistance = 200;

/// Conversion factor from degrees to radians
double DegreesToRadians = 3.1415/180;

/// The range from the center of a Program that is considered to be clicking
/// on the Program
const double ProgramHitRange = 15.0;

/// Conversion constant from Radians to Degrees
const double RadiansToDegrees = 180/3.14159265;

/// Speed wings flap at
const double SpeedToWingFlaps = 1.0/12.0;

/**
 * Constructor
 * @param game Game this bug is a member of
 * @param program program the bug points to
 * @param isChild bool for is child
 */
RedundancyBug::RedundancyBug(Game *game, std::shared_ptr<Program> program, bool isChild) : Bug(game, RedundancyFlyTop, RedundancyBugNumSpriteImages, program)
{


	mImageBase = game->GetImageFromMap(RedundancyFlyBase);
	mImageLeftWing = game->GetImageFromMap(RedundancyFlyLeftWing);
	mImageRightWing = game->GetImageFromMap(RedundancyFlyRightWing);
	mImageTop = game->GetImageFromMap(RedundancyFlyTop);

	mIsChild = isChild;
}

void RedundancyBug::Draw(std::shared_ptr<wxGraphicsContext> graphics)
{

    if (Bug::GetSplat() && mIsChild)
    {
        graphics->PushState();
        graphics->Translate(GetX(), GetY());
        Item::Draw(graphics);
        graphics->PopState();
    }
    else if (!Bug::GetMissed())
    {
        graphics->PushState();
        graphics->Translate(GetX(), GetY());
        graphics->Rotate(Bug::GetRotation());

        if (mLeftWingBitmap.IsNull() || mRightWingBitmap.IsNull() || mTopBitmap.IsNull() || mBaseBitmap.IsNull())
        {
            mBaseBitmap = graphics->CreateBitmapFromImage(*mImageBase);
            mLeftWingBitmap = graphics->CreateBitmapFromImage(*mImageLeftWing);
            mRightWingBitmap = graphics->CreateBitmapFromImage(*mImageRightWing);
            mTopBitmap = graphics->CreateBitmapFromImage(*mImageTop);
        }

        double BaseWid = mImageBase->GetWidth();
        double BaseHit = mImageBase->GetHeight();

        double LeftWingWid = mImageLeftWing->GetWidth();
        double LeftWingHit = mImageLeftWing->GetHeight();

        double RightWingWid = mImageRightWing->GetWidth();
        double RightWingHit = mImageRightWing->GetHeight();

        double TopWid = mImageTop->GetWidth();
        double TopHit = mImageTop->GetHeight();

        int WingOffset = 0;

        graphics->DrawBitmap(mBaseBitmap, -BaseWid / 2, -BaseHit / 2, BaseWid, BaseHit);

        for (int i = 1; i <= NumberOfSetsOfWings; ++i)
        {
			graphics->PushState();
			graphics->Translate(FirstWingSetX + WingOffset , -WingSetY);
			graphics->Rotate(mWingRotation);

			graphics->DrawBitmap(mLeftWingBitmap, -LeftWingWid / 2, - LeftWingHit / 2,
                             LeftWingWid, LeftWingHit);
			graphics->PopState();

			graphics->PushState();
			graphics->Translate(FirstWingSetX + WingOffset, WingSetY);
			graphics->Rotate(-mWingRotation);

            graphics->DrawBitmap(mRightWingBitmap, - RightWingWid / 2,
                             -RightWingWid / 2, RightWingWid, RightWingHit);
			graphics->PopState();
            WingOffset += WingSetXOffset;
        }

        graphics->DrawBitmap(mTopBitmap, -TopWid / 2, -TopHit / 2, TopWid, TopHit);

        graphics->PopState();
    }
}

/**
 * On a left click mouse event
 */
void RedundancyBug::OnLeft()
{
    if (!Bug::GetMissed() && !mIsChild)
    {
        Bug::OnLeft();
		double xPosition = GetX();
		double yPosition = GetY();
		srand(GetX()/(GetY()+1));
		int numberOfBugs = rand() % 4 + 2;
		for (int i = 1; i <= numberOfBugs; i++)
		{
			shared_ptr<RedundancyBug> childBug = make_shared<RedundancyBug>(GetGame(), GetProgram(), true);
			childBug->SetSpeed(GetSpeed());
			int j = 0;
			do
			{
				srand(j + GetX() + i * GetSpeed());
				int angle = rand() % 360;
				srand(j + GetX() + i * GetX()+1);
				int offset = (rand() % 400) - SpawnDistance;
				xPosition = GetX() + (offset) * cos(angle * DegreesToRadians);
				yPosition = GetY() + (offset) * sin(angle * DegreesToRadians);
				j++;
			}
			while (abs(xPosition - GetProgram()->GetX()) < ProgramHitRange
				&& abs(yPosition - GetProgram()->GetY()) < ProgramHitRange);


			childBug->SetLocation(xPosition, yPosition);
			childBug->SetRotation(atan2(((GetProgram()->GetY())-GetY()),((GetProgram()->GetX())-GetX())) * RadiansToDegrees);

			shared_ptr<Item> item = static_pointer_cast<Item>(childBug);
			GetGame()->AddItem(item);
			GetGame()->IncrementBugCounter();
		}


		Missed();
    }
	else if (!Bug::GetMissed())
	{
		Bug::OnLeft();
		Item::SetImage(RedundancyFlySplat);
		//Missed();
	}
}

void RedundancyBug::Update(double elapsed) {
    Bug::Update(elapsed);
	if (mWingRotation > WingRotateStart)
	{
		mWingsUp = false;
	}
	else if (mWingRotation < -WingRotateEnd)
	{
		mWingsUp = true;
	}
	if (mWingsUp && GetSpeed() > 0)
	{
		mWingRotation += SpeedToWingFlaps * GetSpeed() * elapsed;
	}
	else if (GetSpeed() > 0)
	{
		mWingRotation -= SpeedToWingFlaps * GetSpeed() * elapsed;
	}
}
