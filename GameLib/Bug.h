/**
 * @file Bug.h
 * @author Seth Darling, Colton Leslie
 *
 *
 */

#ifndef PROJECT1_GAMELIB_BUG_H
#define PROJECT1_GAMELIB_BUG_H

#include "Item.h"
#include "Program.h"

/**
 * Bug Class
 */
class Bug : public Item
{
private:
	/// Speed of Bug
	double mSpeed = 0;

	/// Rotation of Bug in degrees
	double mRotation = 0;

	/// Time since last sprite sheet change
	double mSpriteSheetTime = 0;

    /// Time that this bug starts to move
    double mStart = 0;

    /// Program that the bug is moving towards
    std::shared_ptr<Program> mProgram;

    /// Boolean showing if a bug has been killed
    bool mSplat = false;

    /// Boolean showing if a bug has been missed
    bool mMissed = false;

protected:
	Bug(Game *game, std::wstring filename, int numOfSheets, std::shared_ptr<Program> program);

public:
    /// Default constructor (disabled)
    Bug() = delete;

    /// Copy constructor (disabled)
    Bug(const Bug &) = delete;

    /// Assignment operator
    void operator=(const Bug &) = delete;

    /**
     * Getter for mRotation
     * @return value mRotation
     */
    double GetRotation() { return mRotation; }

	virtual void Update(double elapsed) override;
	/**
     * Draw overide for Bug
     * @param graphics shared ptr for wxGraphics context
     */
    void Draw(std::shared_ptr<wxGraphicsContext> graphics) override;

	/**
 	 * Load override for Bug
 	 * @param node XML node
     */
    void XmlLoad(wxXmlNode* node) override;

    /**
     * On a left click mouse event
     */
    virtual void OnLeft() override;

    /**
     * Getter for mSplat
     * @return value of mSplat
     */
    bool GetSplat() { return mSplat; }

    /**
     * Getter for mSplat
     * @return value of mSplat
     */
    bool GetMissed() { return mMissed; }

    /**
     * Setter for mSplat
     * @param splat value mSplat is to be set to
     */
    void SetSplat(bool splat) { mSplat = splat; }

    /**
     * Getter for mProgram
     * @return value of mProgram
     */
    std::shared_ptr<Program> GetProgram() { return mProgram; }

	/**
     * Sets mMissed to true
     */
    void Missed();

	/**
  	 * Returns value of bug's speed in pixel's per second
 	 * @return mSpeed value of bug's speed
 	 */
	double GetSpeed() { return mSpeed; }

	/**
	 * Sets Bug speed to accepted value
	 * @param speed Value to set Bug's speed to
	 */
	void SetSpeed(double speed) { mSpeed = speed; }

	/**
	 * Sets Bug rotation to accepted value
	 * @param rotation Value to set Bug's rotation to
	 */
	void SetRotation(double rotation) { mRotation = rotation; }

};

#endif //PROJECT1_GAMELIB_BUG_H
