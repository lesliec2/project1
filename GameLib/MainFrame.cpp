/**
 * @file MainFrame.cpp
 * @author Arul Srivastava
 */

#include "pch.h"
#include "ids.h"
#include "MainFrame.h"
#include "GameView.h"
#include "Scoreboard.h"


/**
 * Initialize the MainFrame window.
 */
void MainFrame::Initialize() {
    Create(nullptr, wxID_ANY, L"wxGraphicsGameContext", wxDefaultPosition, wxSize(1000, 800));

    auto sizer = new wxBoxSizer(wxVERTICAL);

    mGameView = new GameView();
    mGameView->Initialize(this);

    sizer->Add(mGameView, 1, wxEXPAND | wxALL);

    SetSizer(sizer);
    Layout();

    auto menuBar = new wxMenuBar();

    auto fileMenu = new wxMenu();
    auto levelMenu = new wxMenu();
    auto viewMenu = new wxMenu();
    auto helpMenu = new wxMenu();

    menuBar->Append(fileMenu,L"&File");
    menuBar->Append(levelMenu,L"&Levels");
    menuBar->Append(viewMenu, L"&View");
    menuBar->Append(helpMenu, L"&Help");

    fileMenu->Append(wxID_EXIT, "E&xit\tAlt-X", "Quit this program");
    levelMenu->Append(IDM_LEVEL0, L"&Level 0", L"Open level 0");
    levelMenu->Append(IDM_LEVEL1, L"&Level 1", L"Open level 1");
    levelMenu->Append(IDM_LEVEL2, L"&Level 2", L"Open level 2");
    levelMenu->Append(IDM_LEVEL3, L"&Level 3", L"Open level 3");
    viewMenu->Append(IDM_SHRINK, L"&Shrink", L"Make the game shrink in size");
    helpMenu->Append(wxID_ABOUT, "&About\tF1", "Show about dialog");

    SetMenuBar( menuBar );

    CreateStatusBar( 1, wxSTB_SIZEGRIP, wxID_ANY );

    // Recently used files
    mHistory.Load(mConfig);
    mHistory.UseMenu(fileMenu);
    mHistory.AddFilesToMenu();

    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::OnExit, this, wxID_EXIT);
    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::OnAbout, this, wxID_ABOUT);
    Bind(wxEVT_CLOSE_WINDOW, &MainFrame::OnClose, this);
}

/**
 * Exit menu option handlers
 * @param event
 */
void MainFrame::OnExit(wxCommandEvent &event)
{
    Close(true);
}

/**
 * Application about box menu handler
 */
void MainFrame::OnAbout(wxCommandEvent &)
{
    wxMessageBox(L"Welcome to the SquishBug game, hope you enjoy!",
                 L"About the Game",
                 wxOK | wxCENTRE,
                 this);
}

/**
 * Handle a close event. Stop the animation and destroy this window.
 * @param event The Close event
 */
void MainFrame::OnClose(wxCloseEvent &event)
{
    mGameView->Stop();
    Destroy();
}
