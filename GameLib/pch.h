/**
 * @file pch.h
 * @author Seth Darling
 */

#ifndef GAME_PCH_H
#define GAME_PCH_H

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include <wx/xml/xml.h>
#include <wx/graphics.h>

#endif //GAME_PCH_H
