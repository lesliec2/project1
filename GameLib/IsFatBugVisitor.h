/**
 * @file IsFatBugVisitor.h
 * @author Colton Leslie
 * @author Arul Srivastava
 *
 * Visitor class that checks if a bug is a FatBug
 */

#ifndef PROJECT1_ISFATBUGVISITOR_H
#define PROJECT1_ISFATBUGVISITOR_H

#include "ItemVisitor.h"
#include "Bug.h"
#include "GarbageBug.h"
#include "NullBug.h"
#include "FatBug.h"

/**
 *  Visitor class for all the fat bugs
 */
class IsFatBugVisitor : public ItemVisitor{
private:
    /// The FatBug that has been visited
    FatBug* mBug = nullptr;

    /// Bool saying if an Item is a FatBug or not
    bool mIsFatBug = false;
public:
    void VisitGarbage(GarbageBug* garbageBug) override;

    void VisitNull(NullBug* nullBug) override;

    /**
     * Get the pointer to the FatBug
     * @return a pointer to the FatBug
     */
    FatBug* GetBug() { return mBug; }

    /**
     * Check if this object is FatBug
     * @return true or false depending on if it is a FatBug or not
     */
    bool GetIsFatBug() { return mIsFatBug; }
};


#endif //PROJECT1_ISFATBUGVISITOR_H
