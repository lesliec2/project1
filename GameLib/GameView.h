/**
 * @file GameView.h
 * @author Colton Leslie
 *
 * Class that implements the child window our program draws in.
 *
 * The window is a child of the main frame, which holds this
 * window, the menu bar, and the status bar.
 */

#ifndef PROJECT1_GAMEVIEW_H
#define PROJECT1_GAMEVIEW_H

#include "Game.h"

/**
 * View class for our game
 */
class GameView : public wxWindow{
private:
    /// Game to create a view class for. 
    Game mGame;
    void OnPaint(wxPaintEvent& event);

    /// The item that the GameView grabbed last.
    std::shared_ptr<Item> mGrabbedItem;

	/// The timer that allows for animation
	wxTimer mTimer;

	/// Stopwatch used to measure elapsed time
	wxStopWatch mStopWatch;

	/// The last stopwatch time
	long mTime = 0;
public:
    void Initialize(wxFrame *mainFrame);
	void OnTimer(wxTimerEvent &event);
	/**
 	 * Onshrink function
     * @param event (wxCommand Event for Onshrink)
     */
	void OnShrink(wxCommandEvent& event);
	/**
	 * Onlevel0 function, loads in lvl 0
	 * @param event
	 */
	void OnLevel0(wxCommandEvent& event);
	/**
	 * Onlevel1 function, loads in lvl 1
	 * @param event
	 */
    void OnLevel1(wxCommandEvent& event);
	/**
	 * Onlevel2 function, loads in lvl 2
	 * @param event
	 */
	void OnLevel2(wxCommandEvent& event);
	/**
	 * Onlevel3 function, loads in lvl 3
	 * @param event
	 */
	void OnLevel3(wxCommandEvent& event);
	/**
	 * Onleftdown function, left click mouse command
	 * @param event
	 */
	void OnLeftDown(wxMouseEvent& event);


    /**
     * Stop the timer so the window can close
     */
    void Stop() { mTimer.Stop(); }
	/**
 	 * On left double click event handler
 	 * @param event wxMouseEvent
 	 */
	void OnLeftDoubleClick(wxMouseEvent &event);
};


#endif //PROJECT1_GAMEVIEW_H
