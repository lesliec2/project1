/**
 * @file Scoreboard.cpp
 * @author John Schafer
 */

#include "pch.h"
#include "Scoreboard.h"
#include <wx/frame.h>
#include "Game.h"
#include "Item.h"

using namespace std;

/// Score font size to use
const int ScoreSize = 85;

/// Label for score font size to use
const int LabelSize = 40;

/// The font color to use
const wxColour FontColor = wxColour(0, 200, 200);

/// Left score X location. The right score is
/// the width minus this value.
const int LeftScoreX = 150;

/// Score Y location
const int ScoreY = 20;

/// Score label Y location
const int ScoreLabelY = 100;

/// Width of our Game
const int Width = 1250;

/// Filename for program
const wstring filename = L"laptop.png";

/**
 * Constructor
 * @param game Game this scoreboard is a member of
 */
Scoreboard::Scoreboard(Game *game)
{
    mMissed = 0;
    mOops = 0;
    mFixed = 0;
}

/**
 * Draws the statistics on the scoreboard
 * @param graphics allows this function to draw in the main window
 */
void Scoreboard::Draw (std::shared_ptr<wxGraphicsContext> graphics)
{
    wxFont labelFont(wxSize(0, LabelSize),
                     wxFONTFAMILY_SWISS,
                     wxFONTSTYLE_NORMAL,
                     wxFONTWEIGHT_BOLD);

    // Need to go to helproom about how to center text
	graphics->SetFont(labelFont, FontColor);
	graphics->DrawText(L"Fixed", LeftScoreX-30, ScoreLabelY);
	graphics->DrawText(L"Missed", Width/2 - 42, ScoreLabelY);
	graphics->DrawText(L"Oops", Width-LeftScoreX - 30, ScoreLabelY);

    wxFont scoreFont(wxSize(0, ScoreSize),
                     wxFONTFAMILY_SWISS,
                     wxFONTSTYLE_NORMAL,
                     wxFONTWEIGHT_BOLD);

	graphics->SetFont(scoreFont, FontColor);
	graphics->DrawText(to_wstring(mFixed), LeftScoreX, ScoreY);
	graphics->DrawText(to_wstring(mMissed), Width/2, ScoreY);
	graphics->DrawText(to_wstring(mOops), Width - LeftScoreX, ScoreY);
}

void Scoreboard::Clear()
{
    mMissed = 0;
    mOops = 0;
    mFixed = 0;
}