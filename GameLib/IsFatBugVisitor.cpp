/**
 * @file IsFatBugVisitor.cpp
 * @author Colton Leslie
 */

#include "pch.h"
#include "IsFatBugVisitor.h"

/**
 * Visit a GarbageBug object
 * @param garbageBug GarbageBug item we are visiting
 */
void IsFatBugVisitor::VisitGarbage(GarbageBug *garbageBug)
{
    mIsFatBug = garbageBug->GetFat();
    mBug = garbageBug;
}

/**
 * Visit a NullBug object
 * @param nullBug NullBug item we are visiting
 */
void IsFatBugVisitor::VisitNull(NullBug *nullBug)
{
    mIsFatBug = nullBug->GetFat();
    mBug = nullBug;
}
