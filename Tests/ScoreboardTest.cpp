/**
 * @file BugTest.cpp
 * @author Colton Leslie
 */

#include <pch.h>
#include "gtest/gtest.h"
#include <Game.h>
#include <Scoreboard.h>

/** Mock class for testing the class Item */
class ScoreBoardMock : public Scoreboard {
public:
    //, const std::wstring &filename, const int numOfSheets)
    ScoreBoardMock(Game *game) : Scoreboard(game) {}

    //void Draw(std::shared_ptr<wxGraphicsContext> graphics) override {}
};

TEST(ScoreboardTest, Construct)
{
    Game game;
    ScoreBoardMock scoreboard(&game);
}

TEST(ScoreboardTest, GetterSetters)
{
    Game game;
    ScoreBoardMock scoreboard(&game);

    // Check initial values
    ASSERT_EQ(scoreboard.GetFixed(), 0);
    ASSERT_EQ(scoreboard.GetMissed(), 0);
    ASSERT_EQ(scoreboard.GetOops(), 0);

    // Try new values
    scoreboard.AddFixed();
    scoreboard.AddFixed();
    scoreboard.AddFixed();
    scoreboard.AddFixed();
    scoreboard.AddFixed();
    scoreboard.AddFixed();

    scoreboard.AddMissed();
    scoreboard.AddMissed();
    scoreboard.AddMissed();

    scoreboard.AddOops();
    scoreboard.AddOops();
    scoreboard.AddOops();
    scoreboard.AddOops();

    ASSERT_EQ(scoreboard.GetFixed(), 6);
    ASSERT_EQ(scoreboard.GetMissed(), 3);
    ASSERT_EQ(scoreboard.GetOops(), 4);

    // Clear to make sure the values are reset

    scoreboard.Clear();

    ASSERT_EQ(scoreboard.GetFixed(), 0);
    ASSERT_EQ(scoreboard.GetMissed(), 0);
    ASSERT_EQ(scoreboard.GetOops(), 0);
}