/**
 * @file BugTest.cpp
 * @author Colton Leslie
 */

#include <pch.h>
#include "gtest/gtest.h"
#include <Game.h>
#include <Bug.h>
#include <FeatureBug.h>
#include <NullBug.h>
#include <RedundancyBug.h>
#include <GarbageBug.h>

using namespace std;

TEST(BugTest, Construct)
{
    Game game;
    shared_ptr<Program> program = make_shared<Program>(&game);
    NullBug nullBug(&game, program);
    GarbageBug garBug(&game, program);
    RedundancyBug redunBug(&game, program);
    FeatureBug featureBug(&game, program);
}