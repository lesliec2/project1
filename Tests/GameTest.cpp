/**
 * @file GameTest.cpp
 * @author Connor Chapman
 */

#include <pch.h>
#include "gtest/gtest.h"
#include <Game.h>
#include <FeatureBug.h>
#include <NullBug.h>
#include <RedundancyBug.h>
#include <GarbageBug.h>
#include <typeinfo>
#include <wx/filename.h>
#include <fstream>
#include <string>
#include <streambuf>

using namespace std;

class GameTest : public ::testing::Test {

};

TEST_F(GameTest, Construct)
{
    Game game;
}

TEST_F(GameTest, Load) {

    Game game0;
	Game game1;
    Game game2;
	Game game3;

	// Load in level Zero xml file that was given to us
	game0.Load(L"Data/level0.xml");

	//Tests that the number of bugs and programs is correct
	ASSERT_EQ(game0.GetNumBugs(),2);
	ASSERT_EQ(game0.GetNumPrograms(),1);

    // Load in level One xml file that was given to us
    game1.Load(L"Data/level1.xml");

	// Tests that the number of bugs and programs is correct
	ASSERT_EQ(game1.GetNumBugs(),12);
	ASSERT_EQ(game1.GetNumPrograms(),1);

	// Load in level two xml file that was given to us
	game2.Load(L"Data/level2.xml");

	//Tests that the number of bugs and programs is correct
	ASSERT_EQ(game2.GetNumBugs(),24);
	ASSERT_EQ(game2.GetNumPrograms(),3);

	// Load in level three xml file that was given to us
	game3.Load(L"Data/level3.xml");


	//Tests that the number of bugs and programs is correct
	ASSERT_EQ(game3.GetNumBugs(),15);
	ASSERT_EQ(game3.GetNumPrograms(),2);
}