/**
 * @file FatBugTest.cpp
 * @Author Colton
 */

#include <pch.h>

#include "gtest/gtest.h"

 // FATBUG CLASS HAS NOT BEEN IMPLEMENTED YET //
 // THIS TESTING CLASS WILL BE MADE WHEN THE  //
 //         FATBUG CLASS HAS BEEN MADE        //

// Test for constructor
TEST(FatBugTest, Construct)
{

}

// Test for checking if the Fatbug is properly displaying
// and using the IDE popup
TEST(FatBugTest, IDE_Testing)
{
}

// Test for checking if the Fatbug is the correct size
TEST(FatBugTest, Size)
{
}