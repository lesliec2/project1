/**
 * @file ItemTest.cpp
 * @author Connor Chapman
 */

#include <pch.h>
#include "gtest/gtest.h"
#include <Item.h>
#include <Game.h>


const std::wstring name = L"laptop.png";
const int sheets = 1;


/** Mock class for testing the class Item */
class ItemMock : public Item {
public:
	//, const std::wstring &filename, const int numOfSheets)
	ItemMock(Game *game) : Item(game, name, sheets) {}
	void Draw(std::shared_ptr<wxGraphicsContext> graphics) override {}
    void Accept(ItemVisitor* visitor) override {};
    void OnLeft() override {};
};

TEST(TestItem, Construct)
{
	Game game;
	ItemMock item(&game);
}


TEST(ItemTest, GettersSetters){
	Game game;
	ItemMock item(&game);

    // Test initial values
    ASSERT_NEAR(0, item.GetX(), 0.0001);
    ASSERT_NEAR(0, item.GetY(), 0.0001);

    //Test SetLocation, GetX, and GetY
	item.SetLocation(10.5, 17.2);
	ASSERT_NEAR(10.5, item.GetX(), 0.0001);
	ASSERT_NEAR(17.2, item.GetY(), 0.0001);

    // Testa second with different values
    item.SetLocation(-72, -107);
    ASSERT_NEAR(-72, item.GetX(), 0.0001);
    ASSERT_NEAR(-107, item.GetY(), 0.0001);
}

// Here is the HitTest tes for when we implement this
TEST(ItemTest, HitTest)
{
    Game game;
    ItemMock bug(&game);

    bug.SetLocation(100,200);
    // Center of the fish should be a true
    ASSERT_TRUE(bug.HitTest(100, 200));

    // Left of the fish
    ASSERT_FALSE(bug.HitTest(10, 200));

    // Right of the fish
    ASSERT_FALSE(bug.HitTest(200, 200));

    // Above the fish
    ASSERT_FALSE(bug.HitTest(100, 0));

    // Below the fish
    ASSERT_FALSE(bug.HitTest(100, 300));
}